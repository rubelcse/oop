<?php
function simple(){
    echo "this is a simple function"."<br>";
}
simple();

function msg(){
    return "this is a return type"."<br>";
}
$a = msg(); //return value akta variable a store kore rakhe hoice
echo $a;

function math(){
    $a = 10;        // this variable is a local variable
    $b = 5;
    return $a + $b;

}
$result = math();
echo "this result is ".$result."<br>";

//parameters pass in a function
function setVal($a = "", $b = ""){ //best practices is ... declard a variable is empty
    $c = $a + $b;
    return $c;
}
$addresult = setVal(25,10);     //pass the argument in a paramerters.
echo "Add the value is ".$addresult."<br>";

//declard a global variable and using inside a function
$name = "rubel";
$age = 24;
function details(){
    global $name, $age;
    //global $age;
    return "$name is a $age years old";
}
echo details();