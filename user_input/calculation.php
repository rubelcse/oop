<?php
if (isset($_POST['submit'])) {

    $firstNumber = $_POST['fnumber'];
    $lastNumber = $_POST['snumber'];

    if (!empty($_POST['fnumber'] && !empty($_POST['snumber']))) {
        class Calculation
        {
            public function math($a,$b){
                $c = $a + $b;
                return $c;
            }
        }
        $obj = new Calculation();
        $result = $obj->math($firstNumber,$lastNumber);
        echo "Result is : ".$result;
    } else {
        echo "Input Field can't be blank.";
    }
}