<?php
class Person {
    public $name = "Rubel";
    public $age;
    public $address;

    public function msg(){
        echo "Name is ".$this->name."<br>";
    }
    public function setData($age, $add){
        $this->age = $age;
        $this->address = $add;
    }
    public function getData(){
        return $this->name." is {$this->age} years old.He lived in {$this->address}"."<br>";
    }

}
$person = new Person();
$person1 = new Person();
$person->name = "Sabbir";
$person->msg();
$person->setData(25,"bogra");
$details = $person->getData(); //catch the return value
echo $details;
$person1->setData(25,"bogra");
$obj2 = $person1->getData();
echo $obj2;

$info = array($person,$person1);
echo "<pre>";
//print_r($info);
foreach ($info as $value){
    echo "<pre>";
    print_r($value);
}
/*for ($i = 0; $i < count($info); $i++){
    echo "<pre>";
    print_r($info[$i]);
}*/